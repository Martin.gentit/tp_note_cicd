# TP de groupe (5pts + 1pt bonus)

Le TP compte pour un quart de la note de l'évaluation du module.

Le QCM compte pour les trois quarts de l'évaluation du module.

Les captures d'écran doivent couvrir l'ensemble de l'écran (pas de capture de fenêtre).


## Création du projet (0.5pt)

Créer un projet public dans Gitlab avec les sources de cette archives.

Notez dans ci-dessous vos noms et prénoms :
 - GENTIT Martin
 - LEFEUVRE Mathis
 - DOLIVET Pierre
 - KERHERVÉ Guillaume

ainsi que l'URL du projet :
 - gitlab.com/Martin.gentit/tp_note_cicd


Dans la suite du TP, vous être libres d'utiliser une chaine d'intégration continue Gitlab ou Jenkins.
Attention : dans le cas de jenkins, la description de votre chaine d'intégration continue doit faire partie des sources.

**Créez un zip par groupe avec l'ensemble des catures d'écran ainsi que ce fichier renseigné (participants, URL).
Le compte-rendu est à déposer dans Pepal.**

## Planification (0.5pt)

Créer un sprint de maintenant à demain (sprint 1)avec les étapes de mise en place d'une chaine d'intégartion continue :
 - compilation ;
 - test ;
 - qualité de code ;
 - packaging du jar ;
 - packaging de la documentation ;

Créer les tickets correspondants (epics, user stories).

Créer un tableau de bord afin de suivre les états des tickets qui ne doivent être que :
 - à faire
 - en cours
 - en test
 - terminé

Les autres états ne doivent pas aparaitre dans le board.

Répartisssez vous les tickets.

0.5pt


## Compilation (1pt)

En respectant gitflow (gestion des branches, compilation, approbation du merge), mettez en place la phase de compilation.

Faites-une capture d'écran de votre planification : compilation_01.jpg ou compilation_01.png.

La commande de comilation depuis un poste utilisateurs est la suivante : 
    - chmod +x mvnw
    - ./mvnw compile -Dmaven.test.skip=true
0.5pt

Une fois la compilation réussie, mettez à jour vos branches ainsi que votre planification.

Faites-une capture d'écran d'écran de votre planification : compilation_02.jpg ou compilation_02.png.

Faites-une capture d'écran de l'arborescence des branches : compilation_03.jpg ou compilation_03.png.

0.5pt


## Test (1pt)

En respectant gitflow, mettez en place la phase des tests.

Faites-une capture d'écran de votre planification : test_01.jpg ou test_01.png.

La commande d'exécution des test unitaires est la suivante : 
    - chmod +x mvnw
    - ./mvnw test

0.5pt

Une fois l'exécution des tests mise en place, mettez à jour vos branches ainsi que votre planification.

Faites-une capture d'écran d'écran de votre planification : test_02.jpg ou test_02.png.

Faites-une capture d'écran de l'arborescence des branches : test_03.jpg ou test_03.png.

0.5pt



## Qualité (1pt)

En respectant gitflow, mettez en place la phase de vérification de la qualité à l'aide de SonarCloud.

Faites-une capture d'écran de votre planification : quality_01.jpg ou quality_01.png.

Mettez en place l'analyse de votre code avec SonarCloud.

0.5pt

Une fois la vérification de la qualité réussie, mettez à jour vos branches ainsi que votre planification.

Faites-une capture d'écran de votre planification : quality_02.jpg ou quality_02.png.

Faites-une capture d'écran de l'arborescence des branches : quality_03.jpg ou quality_03.png.

Faites-une capture d'écran du résultat d'analyse de votre projet sous SonarCloud : quality_04.jpg ou quality_04.png.

0.5pt



## Packaging (1pt)

En respectant gitflow, mettez en place la phase de packging.

Faites-une capture d'écran de votre planification : packaging_01.jpg ou packaging_01.png.

La commande d'exécution des test unitaires est la suivante : 
    - chmod +x mvnw
    - ./mvnw -Dmaven.test.skip=true install

Stockez le résultat target/listing*.jar en tant qu'artifact si vous utilisez Gitlab ou équivalent dans Jenkins.

0.5pt

Une fois le packaging réussi, mettez à jour vos branches ainsi que votre planification.

Faites-une capture d'écran de votre planification : packaging_02.jpg ou packaging_02.png.

Faites-une capture d'écran de l'arborescence des branches : packaging_03.jpg ou packaging_03.png.

Faites-une capture d'écran du Job et de l'artifact créé dans Gitlab ou équivalent : packaging_04.jpg ou packaging_04.png.

0.5pt


## Bonus packaging de la documentation (1pt)

En respectant gitflow, complétez la phase de packging avec la documentation.

Faites-une capture d'écran de votre planification : bonus_01.jpg ou bonus_01.png.

La commande d'exécution des test unitaires est la suivante : 
 - ./mvnw javadoc:javadoc
 - tar cvzf documentation.tgz target/site

La documentation est générée dans l'archive documentation.tgz.

Stockez la en tant qu'artifact si vous utilisez Gitlab ou équivalent dans Jenkins.

0.5pt

Une fois cette étape réussie, mettez à jour vos branches ainsi que votre planification.

Faites-une capture d'écran de votre planification : bonus_02.jpg ou bonus_02.png.

Faites-une capture d'écran de l'arborescence des branches : bonus_03.jpg ou bonus_03.png.

Faites-une capture d'écran du Job et de l'artifact créé dans Gitlab ou équivalent : bonus_04.jpg ou bonus_04.png.

0.5pt
